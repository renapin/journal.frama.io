# Contribuer

N'importe qui peut soumettre ou demander à faire soumettre un nouvel article et contribuer ainsi au partage des idées, du savoir-faire, de l'expérience...
Pour ajouter un article, votre commit doit s'effectuer dans le dossier content/blog et être de la forme `article.md`.
Les images des articles doivent systématiquement être mise dans le dossier `static/images/blog`.
Avant de lancer une merge request, assurez-vous d'avoir testé le bon fonctionnement du site localement à l'aide d'Hugo.

En premier lieu, clonez le dépôt en faisant un `git clone https://framagit/journal/journal.frama.io` et rendez vous dans le dossier de celui-ci.
Faites vos modifications, ajoutez un article par exemple, etc.
Ajoutez vos modifications à l'index git en tapant `git add -A`

Empaquetez ces modifications dans un 'commit' en tapant `git commit -m "Message du commit"`

Enfin, poussez ce commit vers la branche 'master' du dépôt avec la commande `git push`