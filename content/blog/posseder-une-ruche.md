---
title: "Posséder une ruche"
date: 2018-10-31T11:19:14+02:00
publishdate: 2018-10-31T11:19:14+02:00
image: "/images/blog/1.jpg"
tags: ["transition","apiculture","écologie","autonomie"]
comments: false
---
# Posséder une ruche
## Pourquoi ?

Posséder une ruche implique beaucoup de choses dont il faut tenir compte. Tout le monde n'a pas envie d'avoir des abeilles dans son jardin, surtout en prenant en compte les risques d'allergies aux piqûres.
Il faut s'en occuper régulièrement, c'est-à-dire qu'au moins une fois par semaine il vous faudra aller voir vos petites butineuses, cela permet de les habituer à votre présence et de vérifier que tout va bien.

Malgré les contraintesque cela impose, le jeu en vaut la chandelle ! En effet, même si vous ne récolterez pas forcément beaucoup de miel dès la première année, que vous perdez des essaims, faire vivre des abeilles est déja beaucoup.

La population d'abeille en France est en grand déclin depuis une dizaine d'années, à cause des pesticides mais également du redoutable [frelon asiatique](https://frelon-asiatique.frama.site/), pourtant les abeilles, en butinant les fleurs et en emportant du pollen sur leurs pattes, occupent une place centrale dans l'agriculture et le renouvellement des plantes.

## Comment ?

Je vous conseil de commencer par quelques ruches, au minimum trois. Cela vous permettra, si jamais vous constatez la perte d'un des essaims, de comprendre pourquoi celui-ci n'a pas passé l'hiver, pourquoi le second produit plus de miel que les autres, etc.

1 - Quel type de ruche ?

Bien que les ruches de type warré pourraient vous séduire puisque les cadres sont entièrement construit par les abeilles, cela implique un potentiel épuisement des ouvrières et une plus grande lenteur à la construction de la ruche la première année, rendant l'hiver plus difficile à supporter pour l'essaim.

Pour ces raisons, je vous conseil de partir sur une ruche de type Dadant, voir de construire la votre.

2 - Où les installer ?

Si vous avez un grand jardin, vous pouvez peut-être les mettre directement à votre domicile, le plus éloigné possible de la route et des chemins de passage ;)

Dans l'idéal il faudrait que vous ayez un terrain plus ou moins isolé des autres habitations, proche de chez vous, où vous vous rendez régulièrement.

Surélevez  les ruches pour qu'elles ne soient pas en proie aux petits prédateurs fouineurs et à une forte humidité pouvant faire pourrir le bois. 

Pour posséder une ruche vérifiez la réglementation dans votre commune quand à la distance minimale avec la voie publique par exemple et assurez-vous de déclarer celle-ci ([pour plus d'informations je vous renvoi à la page des services publics à ce sujet](https://www.service-public.fr/professionnels-entreprises/vosdroits/F24392)).

**Données techniques pour l'installation de ruches**

  * Superficie : 20 m² pour 5 ruches, environ 5 m² pour 1 ou 2 ruches.
  * Distances de sécurité : définies par le préfet ou le maire.
  * Accessibilité : prévoir un accès praticable toute l’année avec une brouette.
  * Luminosité : éviter les lieux ombragés et humides, favorisant les maladies.
  * Supports : 2 parpaings (celui placé à l’arrière étant surélevé de 3 cm). 
  * Abreuvoir : à proximité, avec flotteurs ou cailloux pour prévenir les noyades.

**Les meilleurs emplacements et orientations d'une ruche**

  * Choisissez un site à proximité de fleurs, sur un terrain débroussaillé, où quelques arbres ou rochers serviront de repères aux butineuses
  * Ne disposez pas vos ruches en lignes, car cela favorise la dérive des ouvrières.
  * Si, en plaine, vous pouvez placez vos ruchers en croix , évitez toujours de croiser les entrées.
  * L’exposition sud-est est préférable et il faut les abriter des vents dominants par un talus ou une haie.

3 - Matériel

Un peu de matériel vous sera nécéssaire.
Au minimum ayez une combinaison intégrale blanche, lors d'intervention sur la ruche vous devez en être vétu de pieds en cape.

Un enfumoir est conseillé, il permet de faire croire aux abeilles à un incendies mais en même temps la fumée brouillera les signaux avec lesquels elles communiquent.

Des panneaux de bois ou des pièces de rechange pour votre ruche sont également indispensable dans le cas où votre ruche subi quelques dégats.

4 - Se procurer le matériel

Vous pouvez trouver des sites proposant la vente du matériel requis en faisant [une simple recherche sur internet](https://www.qwant.com/?client=brz-moz&q=mat%C3%A9riel+apiculture).

Essayez également de voir autour de chez vous s'il n'y a pas un apiculteur pouvant vous préter ou vous vendre du matériel et des essaims nouveau.

Je vous conseille vraiment de prendre avec un apiculteur, de faire des stages, etc. cela vous permettra d'apprendre plus amplement les fondamentaux de l'apiculture et de profiter d'une plus grande expérience. N'hésitez pas à vous renseignez sur le sujet en lisant des livres dessus.

5 - S'en occuper

Les abeilles sont relativement autonomes, néanmoins il faudra vous en occuper un minimum, ne serait-ce que pour constater leur bonnes conditions, et ceci de façon hebdomadaire. Cela contribura également à les habituer à votre présence.

La première année elle ne produiront qu'à peine une quantité suffisante pour leurs besoins, donc cette année il est peu probable que vous en récolterez. Lorsque vos ruches produiront suffisamment de miel vous pourrez en récupérer.

Pour ce faire, vous devez bien sûr vous couvrir de votre vêtement de protection de la tête aux pieds, comme à chaque intervention. Vous pouvez vous aidez d'un enfumoir pour les calmer.

Vous ouvrez alors votre ruche et repérez un cadre plein de miel (attention à ne pas prendre un cadre emplis de larves !) puis vous le sortez délicatement. Les abeilles présentes dessus vont le quitter petit-à-petit pour rejoindre le reste de la ruche.

## Production

Les abeilles produisent pleins de substances différentes, ainsi vous pouvez espérer obtenir : 

- Du miel, nectar de fleurs transformé dans le jabot des abeilles grâce à certaines enzymes
- De la propolis, résine transformée là aussi par des enzymes, elle est utilisée par les abeilles comme mortier pour colmater, isoler, renforcer les zones défectueuses, protéger l'entrée de la ruche, etc.
- De la gelée royale, sécrétion des nourrices pour former une nouvelle reine (forcer la réalisation de celle-ci par les abeilles est plus compliqué)
- De la cire, matière première utilisée par les abeilles pour la construction de leurs ruches
- Du pollen, qui entre dans la composition de la gelée royale

Vous pouvez récupérer le miel directement d'un cadre en le pressant très fortement avec les mains, cela ceux convenir si vous n'avez que quelques ruches. Vous pouvez aussi utilisez un extracteur qui vous extraira le miel pour un petit effort mécanique.

_N'utilisez PAS de centrifugeuse qui altère les caractéristiques du miel en le faisant tourner à très forte vitesse, impliquant une forte augmentation de temérature._

Enfin, filtrez le miel à votre convenance à l'aide de filtres de plus en plus fin. Cela n'est pas obligatoire si votre miel extrait est relativement "propre".

## Risques

Le principale fléau des vos abeilles sera le [Varroa](https://fr.wikipedia.org/wiki/Varroa) qui parasite vos abeilles. L'autre principal prédateur est le frelon asiatique (Vespa Velutina) qui cherchent à faire leurs réserves à partir de la fin du mois d'août, début septembre. Il vous causera de très nombreuses pertes, alors faites le maximum pour protégez vos butineuses : poses de pièges, recherche de nids...

Si jamais vous trouvez un nid de frelon asiatique ne vous en approchez pas à moins de 20 mètres et contactez immédiatement les autorités locales chargés de ce travail, les pompiers peuvent également s'en occuper.

L'hiver est le second facteur à prendre en compte. Pour que vos abeilles passent l'hiver il faudra en effet qu'elles ai récoltées assez de miel pour leur fournir une source d'énergie suffisante pour maintenir une chaleur constante dans la rûche, autrement vous pouvez tentez d'isoler un minimum celle-ci.

Si vous constatez que votre ruche n'a pas eu une assez bonne récolte pour avoir suffisamment de miel pour l'hiver, le mieux est de les nourrir avec du miel, pas du sirop ! Et idéalement avec leur propre miel

___

J'espère que cela vous permettra d'envisager d'installer une ruche chez vous :)

Si vous ne pouvez tout de même pas vous occuper d'une ruche vous même, vous pouvez parrainer une rûche grace à l'initiative [un toît pour les abeilles](https://www.untoitpourlesabeilles.fr/) !

En tant qu'apiculteur amateur vous pouvez également vous faire parrainer vos ruches, ce qui vous permettra d'amortir leur coût.

___

**Liens utiles :**

https://fr.wikipedia.org/wiki/Apiculture

http://www.cotemaison.fr/jardin/ruche-abeille-chez-soi-les-5-choses-a-savoir_14241.html
