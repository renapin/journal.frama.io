---
title: "Contact"
---

## Qui sommes nous ?
Ce journal est co-écrit bénévolement par une communauté de rédacteurs pendant leur temps libre, cela peut-être vous également. Nous ne sommes pas une entité à part entière, simplement des personnes issus de tout horizons s'étant rassemblés pour proposer leur idées d'alternatives et les mettres en commun.

Le contenu de ce site est hébergé sur notre dépôt <a href="https://framagit.org/journal/journal.frama.io">Framagit</a> où nous gérons tout les apports des différents contributeurs.
## Nous contacter
Vous pouvez nous contacter via notre salon matrix **<a href="https://matrix.to/#/#journal:matrix.org">#journal:matrix.org</a>** ou par mail **<a href="mailto:journal@albakham.me">journal[ AT ]albakham.me</a>** pour toutes demandes de renseignements

Nous possédons également un compte Mastodon <a href="https://mstdn.io/@journal">@journal@mstdn.io</a> que vous pouvez suivre si vous le souhaitez :)