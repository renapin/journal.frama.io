<img src="https://img.shields.io/liberapay/receives/Journal.svg?logo=liberapay">


# Journal

Ceci est le code du journal écosphère, accessible et modifiable par tous et pour tous.

Le site est accessible à l'adresse https://journal.frama.io
N'hésitez pas à contribuer financièrement au projet sur [Liberapay](https://liberapay.com/journal/)